package sopra.project.ipi.api.recipe.services;

import sopra.project.ipi.api.recipe.models.Ingredient;
import sopra.project.ipi.api.recipe.models.Recipe;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * Calculer et récupérer des infos sur une recette.
 */
public class RecipeInfoService {

    private static String withQuotes(String value) {
        return "'"+value+"'";
    }

    private static String instructionFor(Ingredient ingredient) {
        StringBuilder instruction = new StringBuilder();
        return instruction
                .append("Ajouter").append(" ")
                .append(ingredient.getQuantity())
                .append(ingredient.getUnit())
                .append(" de ")
                .append(ingredient.getName())
                .toString();
    }
    public static String getSummaryFor(Recipe recipe) {
        StringBuilder summary = new StringBuilder();
        summary.append("C'est une recette qui s'appelle ");
        summary.append(withQuotes(recipe.getTitle()));
        summary.append(" dont l'auteur est ");
        summary.append(withQuotes(recipe.getAuthor()));
        if (recipe.getPublished()) {
            summary.append(" publiée le ");
            summary.append(recipe.getPublication_date());
        }
        summary.append(" dont voici les étapes dans le désordre : ");

        // List<Ingredient> --> List<String>
        // on transforme chaque ingrédient en phrase (instructionFor)
        List<String> instructions = recipe.getIngredients().stream().map(i -> instructionFor(i))
                .collect(Collectors.toList());
        summary.append(String.join(" - ", instructions));

        // Ou bien plus classiquement...
        // recipe.getIngredients().forEach(i -> summary.append(instructionFor(i)));

        return summary.toString();
    }


    private static Integer getCaloriesFor100g(Ingredient ingredient) {
        Integer result = 0;
        HashMap<String, Integer> caloriesMap = new HashMap<>(){{
            put("farine", 343);
            put("fraise", 30);
            put("sucre", 396);
        }};
        Optional<Map.Entry<String, Integer>> caloriesInfo = caloriesMap.entrySet().stream()
                .filter(entry -> ingredient.getName().contains(entry.getKey())).findFirst();
        if (caloriesInfo.isPresent()) { result = caloriesInfo.get().getValue(); }

        return result;
    }

    public static Integer computeCaloriesFor(Recipe recipe) {
        AtomicInteger caloriesRounded = new AtomicInteger();
        recipe.getIngredients().stream().forEach(ingredient -> {
            // Calories pour une quantité = (calories pour 100g / 100) * la quantité en grammes
            // TODO?: vérifier que l'ingrédient est bien en grammes
            Integer calories = Math.round(getCaloriesFor100g(ingredient) * ingredient.getQuantity() / 100);
            caloriesRounded.addAndGet(calories);
        });

        return caloriesRounded.get();
    }
}
