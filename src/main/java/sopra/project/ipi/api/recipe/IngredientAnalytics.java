package sopra.project.ipi.api.recipe;

public class IngredientAnalytics {

    String name;
    Integer calories;
    Integer proteins;
    Integer carbs;
    Integer lipids;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCalories() {
        return calories;
    }

    public void setCalories(Integer calories) {
        this.calories = calories;
    }

    public Integer getProteins() {
        return proteins;
    }

    public void setProteins(Integer proteins) {
        this.proteins = proteins;
    }

    public Integer getCarbs() {
        return carbs;
    }

    public void setCarbs(Integer carbs) {
        this.carbs = carbs;
    }

    public Integer getLipids() {
        return lipids;
    }

    public void setLipids(Integer lipids) {
        this.lipids = lipids;
    }
}
