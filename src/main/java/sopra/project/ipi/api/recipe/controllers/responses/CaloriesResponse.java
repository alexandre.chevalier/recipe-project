package sopra.project.ipi.api.recipe.controllers.responses;

public class CaloriesResponse {

    String title;

    int calories;

    public CaloriesResponse withTitle(String title) {
        this.title = title;
        return this;
    }

    public CaloriesResponse withCalories(int calories) {
        this.calories = calories;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getCalories() {
        return calories;
    }

    public void setCalories(int calories) {
        this.calories = calories;
    }

}
