package sopra.project.ipi.api.recipe.controllers.responses;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

@JsonDeserialize(builder = SummaryResponse.class)
@JsonPOJOBuilder(buildMethodName = "build", withPrefix = "with")
public class SummaryResponse {
    String input;
    String summary;

    public SummaryResponse() {
        this.input = "";
        this.summary = "";
    }

    public SummaryResponse withInput(String input) {
        this.input = input;
        return this;
    }

    public SummaryResponse withSummary(String summary) {
        this.summary = summary;
        return this;
    }

    public SummaryResponse build() {
        return this;
    }

    public String getInput() {
        return input;
    }

    public void setInput(String input) {
        this.input = input;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }
}
