package sopra.project.ipi.api.recipe.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import sopra.project.ipi.api.recipe.IngredientsReference;
import sopra.project.ipi.api.recipe.controllers.responses.CaloriesResponse;
import sopra.project.ipi.api.recipe.controllers.responses.SummaryResponse;
import sopra.project.ipi.api.recipe.models.Recipe;
import sopra.project.ipi.api.recipe.services.RecipeInfoService;

import java.io.File;
import java.util.HashMap;

@RestController
public class MainController {

    Logger logger = LoggerFactory.getLogger(MainController.class);

    /**
     * Pour tester l'API.
     * @return <code>{"success": true}</code>
     */
    @GetMapping("/")
    public ResponseEntity<HashMap> home() {
        HashMap<String, Boolean> body = new HashMap();
        body.put("success", true);
        return ResponseEntity.ok(body);
    }

    @GetMapping("/ingredients")
    public ResponseEntity<HashMap> ingredients() {
        HashMap<String, Integer> response = new HashMap();
        try {
            File file = ResourceUtils.getFile("data/ingredients.json");
            TypeReference<HashMap<String, Integer>> hashmapStringIntegerRef = new TypeReference<HashMap<String, Integer>>() {};
            ObjectMapper mapper = new ObjectMapper();
            response = mapper.readValue(file, hashmapStringIntegerRef);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return ResponseEntity.ok(response);
    }

    @GetMapping("/ingredients_v2")
    public ResponseEntity<IngredientsReference> ingredients_v2() {
        IngredientsReference response = new IngredientsReference();
        try {
            File file = ResourceUtils.getFile("data/ingredients_v2.json");
            ObjectMapper mapper = new ObjectMapper();
            response = mapper.readValue(file, IngredientsReference.class);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return ResponseEntity.ok(response);
    }

    /**
     * Renvoie les calories d'une recette donnée.
     * @param recipe Une recette à analyser
     * @return CaloriesResponse
     * @throws JsonProcessingException
     */
    @PostMapping("/calories")
    public ResponseEntity<CaloriesResponse> caloriesFrom(@RequestBody Recipe recipe) throws JsonProcessingException {
        int calories = RecipeInfoService.computeCaloriesFor(recipe);
        CaloriesResponse response = new CaloriesResponse()
                .withTitle(recipe.getTitle())
                .withCalories(calories);

        return ResponseEntity.ok(response);
    }

    /**
     * Renvoie un résumé d'une recette donnée.
     * @param recipe Une recette à analyser
     * @return SummaryResponse
     * @throws JsonProcessingException
     */
    @PostMapping("/summary")
    public ResponseEntity<SummaryResponse> summaryFrom(@RequestBody Recipe recipe) throws JsonProcessingException {
        String summary = RecipeInfoService.getSummaryFor(recipe);
        SummaryResponse response = new SummaryResponse()
                .withInput(recipe.toJSON())
                .withSummary(summary);

        return ResponseEntity.ok(response);
    }
}
