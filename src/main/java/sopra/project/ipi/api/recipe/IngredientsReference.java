package sopra.project.ipi.api.recipe;

import java.util.List;

public class IngredientsReference {

    List<IngredientAnalytics> ingredients;

    public List<IngredientAnalytics> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<IngredientAnalytics> ingredients) {
        this.ingredients = ingredients;
    }


}
