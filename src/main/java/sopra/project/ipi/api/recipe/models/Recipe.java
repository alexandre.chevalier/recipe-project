package sopra.project.ipi.api.recipe.models;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Recipe {
    private String title;
    private String author;
    private LocalDate publication_date;
    private Boolean published;
    private List<Ingredient> ingredients;

    public String toJSON() {
        String result = "{}";
        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.registerModule(new JavaTimeModule());
            result = mapper.writeValueAsString(this);
        } catch (JsonProcessingException exception) {
            Logger logger = LoggerFactory.getLogger(Recipe.class);
            logger.error("Impossible de sérialiser Recipe: " + exception.getOriginalMessage());
        }
        return result;
    }

    public Recipe(String title, String author, LocalDate publication_date, Boolean published, List<Ingredient> ingredients) {
        this.title = title;
        this.author = author;
        this.publication_date = publication_date;
        this.published = published;
        this.ingredients = ingredients;
    }

    public Recipe() {
        this.title = "";
        this.author = "";
        this.publication_date = LocalDate.of(0, 1, 1);
        this.published = false;
        this.ingredients = new ArrayList<>();
    }

    public Recipe withTitle(String title) {
        this.title = title;
        return this;
    }

    public Recipe withAuthor(String author) {
        this.author = author;
        return this;
    }

    public Recipe withPublication_date(LocalDate publication_date) {
        this.publication_date = publication_date;
        return this;
    }

    public Recipe withPublished(Boolean published) {
        this.published = published;
        return this;
    }

    public Recipe withIngredients(List<Ingredient> ingredients) {
        this.ingredients = ingredients;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public LocalDate getPublication_date() {
        return publication_date;
    }

    public void setPublication_date(LocalDate publication_date) {
        this.publication_date = publication_date;
    }

    public Boolean getPublished() {
        return published;
    }

    public void setPublished(Boolean published) {
        this.published = published;
    }

    public List<Ingredient> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }
}
